set @str = 'create temporary table r1 as select distinct * from s where sno<3';
prepare op from @str;
execute op;
deallocate prepare op;
set @str = 'create temporary table r2 as select distinct sname, city from r1';
prepare op from @str;
execute op;
deallocate prepare op;
select * from r2;
