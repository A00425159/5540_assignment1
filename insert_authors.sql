insert into author(fname, lname, email) values ('Houda','Abbad','houda.abbad@lycos.com');
insert into author(fname, lname, email) values ('Hideo','Bannai','bannai@inf.kyushu-u.ac.jp');
insert into author(fname, lname, email) values ('Mutlu','Beyazit','beyazit@adt.upb.de');
insert into author(fname, lname, email) values ('Francine','Blanchet-Sadri','blanchet@uncg.edu');
insert into author(fname, lname, email) values ('Janusz','Brzozowski','brzozo@uwaterloo.ca');
insert into author(fname, lname, email) values ('Cezar','Campeanu','cezar@sun11.math.upei.ca');
insert into author(fname, lname, email) values ('Mathieu','Caralp','mathieu.caralp@lif.univ-mrs.fr');
insert into author(fname, lname, email) values ('Pascal','Caron','pascal.caron@univ-rouen.fr');
insert into author(fname, lname, email) values ('Jean-Marc','Champarnaud','Jean-Marc.Champarnaud@univ-rouen.fr');
insert into author(fname, lname, email) values ('Dmitry','Chistikov','dch@mpi-sws.org');
insert into author(fname, lname, email) values ('Christian','Choffrut','Christian.Choffrut@liafa.jussieu.fr');
insert into author(fname, lname, email) values ('Stefano','Crespi-Reghizzi','stefano.crespireghizzi@polimi.it');
insert into author(fname, lname, email) values ('Denis','Debarbieux','denis.debarbieux@inria.fr');
insert into author(fname, lname, email) values ('Pierpaolo','Degano','degano@di.unipi.it');
insert into author(fname, lname, email) values ('Akim','Demaille','akim.demaille@gmail.com');
insert into author(fname, lname, email) values ('Michael','Domaratzki','mdomarat@cs.umanitoba.ca');
insert into author(fname, lname, email) values ('Frank','Drewes','drewes@cs.umu.se');
insert into author(fname, lname, email) values ('Alexandre','Duret-Lutz','adl@lrde.epita.fr');
insert into author(fname, lname, email) values ('Gianluigi','Ferrari','giangi@di.unipi.it');
insert into author(fname, lname, email) values ('Olivier','Gauwin','olivier.gauwin@labri.fr');
insert into author(fname, lname, email) values ('Thomas','Genet','thomas.genet@irisa.fr');
insert into author(fname, lname, email) values ('Daniela','Genova','d.genova@unf.edu');
insert into author(fname, lname, email) values ('Yuri','Gurevich','gurevich@microsoft.com');
insert into author(fname, lname, email) values ('Yo-Sub','Han','emmous@cs.yonsei.ac.kr');
insert into author(fname, lname, email) values ('MdMahbubul','Hasan','shanto86@gmail.com');
insert into author(fname, lname, email) values ('Pierre-Cyrille','Heam','pheam@femto-st.fr');
insert into author(fname, lname, email) values ('Fritz','Henglein','henglein@diku.dk');
insert into author(fname, lname, email) values ('Jan','Holub','Jan.Holub@fit.cvut.cz');
insert into author(fname, lname, email) values ('Markus','Holzer','holzer@informatik.uni-giessen.de');
insert into author(fname, lname, email) values ('Tomohiro','I','tomohiro.i@inf.kyushu-u.ac.jp');
insert into author(fname, lname, email) values ('A.S.M.Sohidull','Islam','sohansayed@gmail.com');
insert into author(fname, lname, email) values ('Masami','Ito','ito@cc.kyoto-su.ac.jp');
insert into author(fname, lname, email) values ('Sebastian','Jakobi','sebastian.jakobi@informatik.uni-giessen.de');
insert into author(fname, lname, email) values ('Jozef','Jirasek','jozef.jirasek@upjs.sk');
insert into author(fname, lname, email) values ('Oscar','Ibarra','ibarra@cs.ucsb.edu');
insert into author(fname, lname, email) values ('Shunsuke','Inenaga','inenaga@inf.kyushu-u.ac.jp');
insert into author(fname, lname, email) values ('Alois','Dreyfus','alois.dreyfus@femto-st.fr');
insert into author(fname, lname, email) values ('Galina','Jiraskova','jiraskov@saske.sk');
insert into author(fname, lname, email) values ('Natasa','Jonoska','jonoska@math.usf.edu');
insert into author(fname, lname, email) values ('Helmut','Jurgensen','hjj@csd.uwo.ca');
insert into author(fname, lname, email) values ('Lila','Kari','lila@csd.uwo.ca');
insert into author(fname, lname, email) values ('Andrzej','Kisielewicz','andrzej.kisielewicz@gmail.com');
insert into author(fname, lname, email) values ('Sang-Ki','Ko','narame7@cs.yonsei.ac.kr');
insert into author(fname, lname, email) values ('Stavros','Konstantinidis','s.konstantinidis@smu.ca');
insert into author(fname, lname, email) values ('Olga','Kouchnarenko','olga.kouchnarenko@femto-st.fr');
insert into author(fname, lname, email) values ('Dexter','Kozen','kozen@cs.cornell.edu');
insert into author(fname, lname, email) values ('Wener','Kuich','kuich@tuwien.ac.at');
insert into author(fname, lname, email) values ('Natalia','Kushik','ngkushik@gmail.com');
insert into author(fname, lname, email) values ('Martin','Kutrib','kutrib@informatik.uni-giessen.de');
insert into author(fname, lname, email) values ('Tristan','LeGall','tristan.le-gall@cea.fr');
insert into author(fname, lname, email) values ('Axel','Legay','axel.legay@inria.fr');
insert into author(fname, lname, email) values ('Pawan','Lingras','pawan.lingras@smu.ca');
insert into author(fname, lname, email) values ('Norma','Linney','norma.linney@smu.ca');
insert into author(fname, lname, email) values ('Sylvain','Lombardy','Sylvain.Lombardy@labri.fr');
insert into author(fname, lname, email) values ('Eva','Maia','emaia@dcc.fc.up.pt');
insert into author(fname, lname, email) values ('Rupak','Majumdar','rupak@mpi-sws.org');
insert into author(fname, lname, email) values ('Andreas','Malcher','andreas.malcher@informatik.uni-giessen.de');
insert into author(fname, lname, email) values ('Andreas','Maletti','andreas.maletti@ims.uni-stuttgart.de');
insert into author(fname, lname, email) values ('Sebastian','Maneth','Sebastian.Maneth@gmail.com');
insert into author(fname, lname, email) values ('Denis','Maurel','denis.maurel@univ-tours.fr');
insert into author(fname, lname, email) values ('Carlo','Mereghetti','mereghetti@di.unimi.it');
insert into author(fname, lname, email) values ('Gianluca','Mezzetti','mezzetti@di.unipi.it');
insert into author(fname, lname, email) values ('Nelma','Moreira','nam@dcc.fc.up.pt');
insert into author(fname, lname, email) values ('Frantisek','Mraz','mraz@ksvi.ms.mff.cuni.cz');
insert into author(fname, lname, email) values ('Paul','Muir','muir@smu.ca');
insert into author(fname, lname, email) values ('Valerie','Murat','valerie.murat@inria.fr');
insert into author(fname, lname, email) values ('Joachim','Niehren','joachim.niehren@inria.fr');
insert into author(fname, lname, email) values ('Lasse','Nielsen','lasse.nielsen.dk@gmail.com');
insert into author(fname, lname, email) values ('Takaaki','Nishimoto','a32b16c4@gmail.com');
insert into author(fname, lname, email) values ('Friedrich','Otto','otto@theory.informatik.uni-kassel.de');
insert into author(fname, lname, email) values ('Beatrice','Palano','palano@dsi.unimi.it');
insert into author(fname, lname, email) values ('Giovanni','Pighizzini','pighizzini@dico.unimi.it');
insert into author(fname, lname, email) values ('Daniel','Prusa','prusapa1@cmp.felk.cvut.cz');
insert into author(fname, lname, email) values ('M.Sohel','Rahman','sohel.kcl@gmail.com');
insert into author(fname, lname, email) values ('Ian','McQuillan','mcquillan@cs.usask.ca');
insert into author(fname, lname, email) values ('George','Rahonis','grahonis@math.auth.gr');
insert into author(fname, lname, email) values ('Bala','Ravikumar','ravi@cs.sonoma.edu');
insert into author(fname, lname, email) values ('Daniel','Reidenbach','D.Reidenbach@lboro.ac.uk');
insert into author(fname, lname, email) values ('Rogerio','Reis','rvr@dcc.fc.up.pt');
insert into author(fname, lname, email) values ('Pierre-Alain','Reynier','pierre-alain.reynier@lif.univ-mrs.fr');
insert into author(fname, lname, email) values ('Jacques','Sakarovitch','sakarovitch@telecom-paristech.fr');
insert into author(fname, lname, email) values ('Michel','Rigo','m.rigo@ulg.ac.be');
insert into author(fname, lname, email) values ('Kai','Salomaa','ksalomaa@cs.queensu.ca');
insert into author(fname, lname, email) values ('Pierluigi','San-Pietro','pierluigi.sanpietro@polimi.it');
insert into author(fname, lname, email) values ('Porter','Scobey','porter.scobey@smu.ca');
insert into author(fname, lname, email) values ('Tom','Sebastian','tom.sebastian@inria.fr');
insert into author(fname, lname, email) values ('Ayon','Sen','ayonsn@gmail.com');
insert into author(fname, lname, email) values ('Geraud','Senizergues','ges@labri.fr');
insert into author(fname, lname, email) values ('Klaus','Sutner','sutner@cs.cmu.edu');
insert into author(fname, lname, email) values ('Marek','Szykula','marek.esz@gmail.com');
insert into author(fname, lname, email) values ('Masayuki','Takeda','takeda@inf.kyushu-u.ac.jp');
insert into author(fname, lname, email) values ('Jean-Marc','Talbot','jean-marc.talbot@lif.univ-mrs.fr');
insert into author(fname, lname, email) values ('Marc','Tommasi','Marc.Tommasi@univ-lille3.fr');
insert into author(fname, lname, email) values ('Mikhail','Volkov','Mikhail.Volkov@usu.ru');
insert into author(fname, lname, email) values ('Bruce','Watson','bruce@fastar.org');
insert into author(fname, lname, email) values ('Matthias','Wendlandt','matthias.wendlandt@informatik.uni-giessen.de');
insert into author(fname, lname, email) values ('Hsu-Chun','Yen','yen@cc.ee.ntu.edu.tw');
insert into author(fname, lname, email) values ('Nina','Yevtushenko','ninayevtushenko@yahoo.com');
insert into author(fname, lname, email) values ('Mohamed','Zergaoui','innovimax@gmail.com');
insert into author(fname, lname, email) values ('Alexander','Okhotin','alexander.okhotin@utu.fi');
