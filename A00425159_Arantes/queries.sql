-- (a) Get the names and locations of the suppliers who have shipped part with pno = 3.

select sname, city from s supplier, sp suppart where supplier.sno = suppart.sno and suppart.pno = 3;
-- +-------+--------+
-- | sname | city   |
-- +-------+--------+
-- | sn1   | London |
-- | sn2   | Paris  |
-- | sn3   | London |
-- +-------+--------+
-- 3 rows in set (0.00 sec)

-- or...
-- select sname, city from s where sno in (select sno from sp where pno = 3);
-- +-------+--------+
-- | sname | city   |
-- +-------+--------+
-- | sn1   | London |
-- | sn2   | Paris  |
-- | sn3   | London |
-- +-------+--------+
-- 3 rows in set (0.00 sec)



-- (b) Get the part numbers and names of parts that have been shipped by suppliers located in Paris with status at least 20.

select p.pno, p.pname from p, sp where p.pno = sp.pno and sp.sno in (select sno from s where city = 'Paris' and status >= 20);
-- +-----+-------+
-- | pno | pname |
-- +-----+-------+
-- |   1 | pn1   |
-- |   2 | pn2   |
-- |   3 | pn3   |
-- |   4 | pn4   |
-- +-----+-------+
-- 4 rows in set (0.01 sec)


-- (c) For each part, show the part number, name, and the number of suppliers who have supplied the part.

select p.pno, p.pname, spag.count_supliers from p, (select pno, count(distinct sno) as count_supliers from sp group by pno) spag
where p.pno = spag.pno;
-- +-----+-------+----------------+
-- | pno | pname | count_supliers |
-- +-----+-------+----------------+
-- |   1 | pn1   |              4 |
-- |   2 | pn2   |              4 |
-- |   3 | pn3   |              3 |
-- |   4 | pn4   |              2 |
-- |   5 | pn5   |              1 |
-- +-----+-------+----------------+
-- 5 rows in set (0.00 sec)



-- (d) For each London supplier who has shipped at least 1000 parts, show the name of the supplier and the total number of parts he/she has shipped.

select (select s.sname from s where s.city = 'London' and s.sno = sp.sno) as sname, sum(qty) from sp group by sno having sum(qty) > 1000;
-- +-------+----------+
-- | sname | sum(qty) |
-- +-------+----------+
-- | sn1   |     1500 |
-- +-------+----------+
-- 1 row in set (0.01 sec)


-- (e) Get the names and cities of the suppliers who have supplied all parts that weigh less than 4 grams.

-- select sno, count(1) as num_parts_supplied from sp, p
-- where p.pno = sp.pno
-- and p.weight < 4
-- group by sno;
-- +-----+-------------------+
-- | sno | num_parts_supplied |
-- +-----+-------------------+
-- |   1 |                 3 |
-- |   2 |                 3 |
-- |   3 |                 3 |
-- |   4 |                 2 |
-- +-----+-------------------+
-- 4 rows in set (0.00 sec)

select s.sname, s.city
from s,
(select count(1) as count_parts_lt4g from p where weight < 4) p4g,
(select sno, count(1) as num_parts_supplied from sp, p where p.pno = sp.pno and p.weight < 4 group by sno) sp4gparts
where s.sno = sp4gparts.sno
-- to make sure it gets only suppliers that supplied ALL parts less than 4g
and p4g.count_parts_lt4g = sp4gparts.num_parts_supplied;
-- +-------+--------+
-- | sname | city   |
-- +-------+--------+
-- | sn1   | London |
-- | sn2   | Paris  |
-- | sn3   | London |
-- +-------+--------+
-- 3 rows in set (0.00 sec)
