#!/bin/bash
#
db="$1"
user="$2"
pass="$3"
#

# connects to mysql and runs the script in create_author_table.sql
mysql -u "$user" --password="$pass" "$db" -e "source create_author_table.sql;"
retcode=$?
if [[ $retcode -eq 0 ]]
then
	echo "-> Created table \"author\" in MySQL database"
else
	echo "-> Create table failed. Retcode = $retcode"
fi

#
echo
# call a small python script that reads the csv and creates insert statements (sed is removing the first line that is the header - could/should remove from the input)
python create_inserts.py | sed 1d > insert_authors.sql

# runs the insert statements created
mysql -u "$user" --password="$pass" "$db" -e "source insert_authors.sql;"
retcode=$?

echo
if [[ $retcode -eq 0 ]]
then
        echo "-> Inserted from authors.csv into mysql table \"authors\""
else
        echo "-> Insert failed. Retcode = $retcode"
fi

# exports the author table contents to a txt (not using into outfile command)
##echo "select * from author;" | mysql -u "$user" --password="$pass" "$db" | sed 's/\t/                /g' > ${user}.txt
echo "select * from author;" | mysql -u "$user" --password="$pass" "$db" > ${user}.txt
retcode=$?
echo
if [[ $retcode -eq 0 ]]
then
	echo "-> Contents of table \"author\" saved in file \"$user.txt\""
else
	echo "-> Export contents of table failed. Retcode = $retcode"
fi

echo "Dropping table author."
mysql -u "$user" --password="$pass" "$db" -e "drop table if exists author;"
echo
echo "-> Deleted table \"author\" from MySQL database. Bye!"
echo

