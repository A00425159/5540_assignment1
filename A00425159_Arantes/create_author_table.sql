-- creates the table author
create table author (_id int not null auto_increment primary key,
                    fname varchar(30),
                    lname varchar(30),
                    email varchar(50)
                    );

